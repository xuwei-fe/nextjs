import React from 'react'
import { initStore } from '../redux/store'
import withRedux from 'next-redux-wrapper'
import Layout from 'layout/BasicLayout'
import Nav from 'layout/Nav'
import Footer from 'layout/Footer'
import MsgCenter from '../components/MsgCenter'
import Head from 'next/head'
import Link from 'next/link'
// 消息中心页面
class App extends React.Component {
    componentDidMount () {
    }
    componentWillUnmount () {
        //clearInterval(this.timer)
    }
    render () {
        return (
            <Layout title={`消息中心`}>
                <Head><link rel="stylesheet" href="/_next/static/style.css" /></Head>
                <Nav arr={5}/>
                <MsgCenter />
                <Footer/>
            </Layout>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        //addCount: bindActionCreators(addCount, dispatch),
        //startClock: bindActionCreators(startClock, dispatch)
    }
}

export default withRedux(initStore, null, mapDispatchToProps)(App)