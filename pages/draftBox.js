import React from 'react'
import { initStore } from '../redux/store'
import withRedux from 'next-redux-wrapper'
import Layout from 'layout/BasicLayout'
import Nav from 'layout/Nav'
import Footer from 'layout/Footer'
import DraftBox from '../components/DraftBox'
import Head from 'next/head'
import Link from 'next/link'
// 草稿箱
class App extends React.Component {
    componentDidMount () {
    }
    componentWillUnmount () {
        //clearInterval(this.timer)
    }
    render () {
        return (
            <Layout title={`草稿箱`}>
                <Head><link rel="stylesheet" href="/_next/static/style.css" /><link rel="stylesheet" href="../styles/antd.min.css" /></Head>
                <Nav arr={5}/>
                <DraftBox />
                <Footer/>
            </Layout>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        //addCount: bindActionCreators(addCount, dispatch),
        //startClock: bindActionCreators(startClock, dispatch)
    }
}

export default withRedux(initStore, null, mapDispatchToProps)(App)