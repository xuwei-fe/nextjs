import React from 'react'
import { initStore } from '../redux/store'
import withRedux from 'next-redux-wrapper'
import Layout from 'layout/BasicLayout'
import Nav from 'layout/Nav'
import Footer from 'layout/Footer'
import About from '../components/About'
import Head from 'next/head'
import Link from 'next/link'
import 'styles/error/error.scss'
class App extends React.Component {
    static getInitialProps({ res, err }) {
        console.log(err)
        const statusCode = res ? res.statusCode : err ? err.statusCode : null;
        return { statusCode }
    }
    goBack(){
        history.go(-1)
    }
    componentDidMount () {

    }
    
    componentWillUnmount () {
        //clearInterval(this.timer)
    }
    
    render () {
        return (
            <Layout title={`错误页面`}>
                <Head><link rel="stylesheet" href="/_next/static/style.css" /></Head>
                <Nav arr={99}/>                
                <div className="errorWrap" >
                
                    <img src={this.props.statusCode =="404" ? '../static/com/404.png':'../static/com/500.png'} />
                    <p>页面找不到了！</p>
                    <div className="btn" onClick={this.goBack}>返回上一页</div>
                </div>
                <Footer/>
            </Layout>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        //addCount: bindActionCreators(addCount, dispatch),
        //startClock: bindActionCreators(startClock, dispatch)
    }
}

export default withRedux(initStore, null, mapDispatchToProps)(App)