import React from 'react'
import { initStore } from '../redux/store'
import withRedux from 'next-redux-wrapper'
import Layout from 'layout/BasicLayout'
import Nav from 'layout/Nav'
import Footer from 'layout/Footer'
import About from '../components/About'
import Head from 'next/head'
import Link from 'next/link'
class App extends React.Component {
    componentDidMount () {

    }
    
    componentWillUnmount () {
        //clearInterval(this.timer)
    }
    
    render () {
        return (
            <Layout title={`关于我们`}>
                <Head><link rel="stylesheet" href="/_next/static/style.css" /><link rel="stylesheet" href="../styles/antd.min.css" /></Head>
                <Nav arr={3}/>                
                <About />
                <Footer/>
            </Layout>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        //addCount: bindActionCreators(addCount, dispatch),
        //startClock: bindActionCreators(startClock, dispatch)
    }
}

export default withRedux(initStore, null, mapDispatchToProps)(App)