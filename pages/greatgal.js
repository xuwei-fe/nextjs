import React from 'react'
import { initStore } from '../redux/store'
import withRedux from 'next-redux-wrapper'
import Layout from 'layout/BasicLayout'
import Nav from 'layout/Nav'
import Greatgal from '../components/Greatgal'
import Head from 'next/head'
import Link from 'next/link'
class App extends React.Component {
    componentDidMount () {

    }
    
    componentWillUnmount () {
        //clearInterval(this.timer)
    }
    
    render () {
        return (
            <Layout title={`成为大咖`}>
                <Head ><link rel="stylesheet" href="/_next/static/style.css" /></Head>
                <Nav arr={1}/>
                <Greatgal/>
            </Layout>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        //addCount: bindActionCreators(addCount, dispatch),
        //startClock: bindActionCreators(startClock, dispatch)
    }
}

export default withRedux(initStore, null, mapDispatchToProps)(App)