import React from 'react'
import { initStore } from '../redux/store'
import withRedux from 'next-redux-wrapper'
import Layout from 'layout/BasicLayout'
import Nav from 'layout/Nav'
import Footer from 'layout/Footer'
import ManageAcount from '../components/ManageAcount'
import Head from 'next/head'
import Link from 'next/link'
// 账号管理页面
class App extends React.Component {
    componentDidMount () {
    }
    componentWillUnmount () {
        //clearInterval(this.timer)
    }
    render () {
        return (
            <Layout title={`账号管理`}>
                <Head>
                    <link rel="stylesheet" href="/_next/static/style.css" />
                    <link rel="stylesheet" href="../styles/antd.min.css" />
                    <script type="text/javascript" charset="utf-8" src="/static/js/cropbox-min.js"> </script>
                </Head>
                <Nav arr={5}/>
                <ManageAcount />
                <Footer/>
            </Layout>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        //addCount: bindActionCreators(addCount, dispatch),
        //startClock: bindActionCreators(startClock, dispatch)
    }
}

export default withRedux(initStore, null, mapDispatchToProps)(App)