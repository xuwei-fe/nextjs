import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import HandleLeft from '../../layout/HandleLeft'
import Content from './Content'
import { getToken } from '/redux/store'
class About extends React.Component {
    constructor(props) {
        
        super(props)
        this.state = {
            currentHot: ''
        }
    }
    upDateValue(value) {
    }
    hotClick(text) {        
    }
    render () {
        var warpDiv = {
            width:"1080px",
            margin:"30px auto",
            overflow:"hidden"
        }
        return (
            <div style={warpDiv}>
                <HandleLeft isChoose={1}/>
                <Content contentId={this.props.contentId} />
            </div>
        )
    }
}

const mapStateToProps = this.props

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(About)