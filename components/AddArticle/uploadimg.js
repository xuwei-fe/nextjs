import React from 'react';
import {Modal, Button,message} from 'antd'
import 'styles/uploadimg/uploadimg.scss'
import fetchData from '/utils/fetchData'
class Uploadimg extends React.Component {
  constructor(props){
    super(props);
    this.state={
        options :
        {
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: '/static/addarticle/baseHeader.png',
            isLoading:false,
            callback:function(){
                //console.log(this.getDataURL());
            }
        }
    }
  }
  componentWillReceiveProps(props){//在组件接收到一个新的 prop (更新后)时被调用。这个方法在初始化render时不会被调用。
    
  }
  componentDidMount(){
    setTimeout(()=>{
        this.cropper = $('.imageBox').cropbox(this.state.options);
        var this_=this;
        $('#file').off('change').on('change', function(){
            var type = this.value.split('.')[this.value.split('.').length-1]
            if(String(type) != 'jpg' && String(type) != 'jpeg' && String(type) != 'png' && String(type) != 'JPG' && String(type) != 'JPEG' && String(type) != 'PNG'){
                message.error('请选择图片');
                this.value=''
                return;   
            }
            var reader = new FileReader();
            reader.onload = function(e) {
                let datajson = Object.assign({},this_.state.options);
                datajson.imgSrc = e.target.result;
                this_.setState({
                    options:datajson
                })
                this_.cropper = $('.imageBox').cropbox(this_.state.options);
            }
            reader.readAsDataURL(this.files[0]);
            $(this).val("");
        })
        $('#btnCrop').on('click', function(){
            
        })
        $('#btnZoomIn').on('click', function(){
            this_.cropper.zoomIn();
        })
        $('#btnZoomOut').on('click', function(){
            this_.cropper.zoomOut();
        })
    },100)
  }
  componentWillMount() {

  }
  handleChange(value){
    
  }
  handleOk = (e) => {
    if(this.state.isLoading){
        return;
    }
    this.setState({
        isLoading:true,
    })
    var img = this.cropper.getDataURL().split(",");
    var imgtype=this.cropper.getBlob().type.split("/")
    //$('.cropped').append('<img src="'+img+'">');
    //     var formData = new FormData();
    //     formData.append('base64file', img[1]);
    //     formData.append('type', 1);
    //     formData.append('originalFilename',"."+imgtype[1]);
    //     reqwest({
    //         url:'image/base64/path/upload',
    //         method: 'post',
    //         processData: false,
    //         data: formData,
    //         headers: {
    //         'Authorization':'holder ' + sessionStorage.getItem('token')
    //         },
    //         success: (respose) => {
    //             this.props.urlFn(respose.respData)
    //         },
    //         error: (respose) => {
                
    //         },
    // });
    var formData = {
        'base64file':img[1],
        'type':'1',
        'originalFilename':"."+imgtype[1]
    }
    fetchData.post(
        'image/base64/path/upload',
        formData,
        {headers: {'Authorization':'bearer '+JSON.parse(sessionStorage.getItem('user')).access_token+''}}
    )
    .then((respose)=>{
        this.props.urlFn(respose.respData)
        this.setState({
            isLoading:false,
        })
    })
    
  }
  handleCancel = (e) => {
    this.props.closeModal();
    this.setState({
        isLoading:false,
    })
  }
  render(){
    return (
        <div>
            <Modal
                width={800}
                title={"图片裁剪"}
                visible={this.props.visible}
                maskClosable={false}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={
                    [<Button key="cancel"  onClick={this.handleCancel.bind(this)} style={{marginRight:"5px",marginBottom:"5px"}}>取消</Button>,
                        
                    this.state.isLoading?
                    <Button key="loading" style={{marginRight:"5px",marginBottom:"5px"}}>上传中...</Button>
                    :
                    <Button key="send" type="primary"  onClick={this.handleOk.bind(this)} style={{marginRight:"5px",marginBottom:"5px"}}>确定</Button>
                    ]
                }
                >
                <div className="container">
                    <div className="imageBox">
                        <div className="thumbBox" style={{width:this.props.width,height:this.props.height,marginTop:-(this.props.height/2),marginLeft:-(this.props.width/2)}}>
                            <div style={{width:1,overflow:'hidden',background:'#ccc',height:this.props.height-2,position:"absolute",left:"50%",top:0}}></div>
                            <div style={{width:this.props.width-2,overflow:'hidden',background:'#ccc',height:1,position:"absolute",left:0,top:"50%"}}></div>
                        </div>
                        <div className="spinner">Loading...</div>
                    </div>
                    <div className="action">
                        <input type="file" id="file" accept="image/png,image/jpeg,image/jpg"/>
                        <input type="button" id="btnZoomIn" value="放大" style={{float:'right',marginLeft:'10px'}}/>
                        <input type="button" id="btnZoomOut" value="缩小" style={{float:'right'}}/>
                    </div>
                </div>
            </Modal>
        </div>

    )
  }
}
export default Uploadimg;