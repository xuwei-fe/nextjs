import React from 'react'
import Link from 'next/link'
import { Pagination } from 'antd';
import fetchData from '/utils/fetchData'
import 'styles/msgCenter/msgCenter.scss'


class Content extends React.Component{
    constructor(props) {
        super(props)
        this.state={
            dataList:[]
        }
        this.dataTotal="";//总条数
        this.startPage = 1;
        this.pageSize = 6//每页条数
    }

    componentDidMount(){
        if(!JSON.parse(sessionStorage.getItem('user'))){
            Router.push({
                pathname: '/index',
            })
        }
        this.getBaseData(1);
    }
    getBaseData(value){
        this.startPage = value;
        fetchData.get('/message/pc/myMessageList?start='+value+'&end='+this.pageSize,
            {headers:{"Authorization":"bearer "+JSON.parse(sessionStorage.getItem('user')).access_token+""}}
        )
        .then((response)=>{
            this.dataTotal = response.respData.count
            this.setState({
                dataList:response.respData.resultData
            })
        })
        .catch((error)=> {
        });
    }
    render(){
        return (
            this.state.dataList.length != 0 ? 
            <div className="msgContent">
                <p className="title">您一共收到 <span>{this.dataTotal}</span> 条系统消息，请注意查看</p>
                <div className="line"></div>
                {/* 消息列表 */}
                <ul className="msgList">
                    {
                        this.state.dataList.map((item,i)=>{
                            return(
                                <li className="li" key={i}>
                                    <div className="time">
                                        <div className="year">
                                            {
                                                item.createTime.substr(0,7)
                                            }
                                        </div>
                                        <div className="day">
                                            {
                                                item.createTime.substr(8,2)
                                            }
                                        </div>
                                    </div>
                                    <div className="con">
                                        <p className="tip">{item.title}<span>
                                            {
                                                item.createTime.substr(11,8)
                                            }
                                            </span>
                                        </p>
                                        <p className="data">{item.content}</p>
                                    </div>
                                </li>  
                            )
                        })
                    }                  
                </ul>
                {/* 分页 */}
                <div className="page">
                    <Pagination current={this.startPage} pageSize={this.pageSize} onChange={this.getBaseData.bind(this)} total={this.dataTotal} />
                </div>           
            </div>:
            <div className="msgContentNodata">
                <img src="../static/com/blank5.png" />
                <p>页面暂无数据</p>
            </div>
        )
    }
}
export default Content




