import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import HandleLeft from '../../layout/HandleLeft'
import Content from './Content'

class About extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentHot: ''
        }       
    }
    upDateValue(value) {
    }
    hotClick(text) {        
    }
    render () {
        var warpDiv = {
            width:"1080px",
            margin:"30px auto",
            overflow:"hidden"
        }
        return (
            <div style={warpDiv}>
                <HandleLeft isChoose={6}/>
                <Content />
            </div>
        )
    }
}

const mapStateToProps = this.props

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(About)