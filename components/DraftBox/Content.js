import React from 'react'
import Link from 'next/link'
import { Pagination,Modal,message } from 'antd';
import Router from 'next/router'
import fetchData from '/utils/fetchData'
import 'styles/draftBox/draftBox.scss'

class Content extends React.Component{
    constructor(props) {
        super(props)
        this.state={
            dataList:[]
        }
        this.dataTotal="";//总条数
        this.startPage = 1;
        this.pageSize = 6//每页条数
    }

    componentDidMount(){
        if(!JSON.parse(sessionStorage.getItem('user'))){
            Router.push({
                pathname: '/index',
            })
        }
        this.getBaseData(1);
    }
    getBaseData(value){
        this.startPage = value;
        fetchData.get('/content/pc/content/query?dataStatus=1&start='+value+'&end='+this.pageSize,
            {headers:{"Authorization":"bearer "+JSON.parse(sessionStorage.getItem('user')).access_token+""}}
        )
        .then((response)=>{
            this.dataTotal = response.respData.count
            this.setState({
                dataList:response.respData.resultData
            })
        })
        .catch((error)=> {
        });
    }
    deleteData(id){
        Modal.confirm({
            content:"确定要执行改操作吗？",
            okText:"确定",
            cancelText:"取消",
            onOk:()=>{
                var data = {
                    contentId:id,
                }
                fetchData.post('/content/pc/content/delete',data,{headers:{"Authorization":"bearer "+JSON.parse(sessionStorage.getItem('user')).access_token+""}})
                .then((response)=>{
                    message.success('删除成功');
                    console.log(this.startPage);
                    if(Math.ceil(this.dataTotal/this.pageSize) == this.startPage && this.dataTotal%this.pageSize == 1 && this.startPage !=1){
                        this.startPage = this.startPage-1
                    }
                    this.getBaseData(this.startPage)
                })
                .catch((error)=> {
                });
            },
            onCancel:()=>{}
        })       
    }
    editData(id){
        Modal.confirm({
            content:"确定要执行改操作吗？",
            okText:"确定",
            cancelText:"取消",
            onOk:()=>{
                Router.push({
                    pathname: '/addArticle',
                    query:{'id':id}
                })
            },
            onCancel:()=>{}
        })  
    }
    render(){
        return (
            this.state.dataList.length != 0 ? 
            <div className="draftBoxContent">
                <ul className="list">
                    {
                        this.state.dataList.map((item,i)=>{
                            return(
                                <li className="li" key={i}>
                                    <div className="top">
                                        <div className="img"> <img src={item.imageAddr} /> </div>
                                        <div className="hover">
                                            <span className="sp1" onClick={this.editData.bind(this,item.contentId)}></span>
                                            <span className="sp2" onClick={this.deleteData.bind(this,item.contentId)}></span>
                                        </div>
                                    </div>
                                    <div className="bottom">
                                        <div className="p1">{item.title}</div>
                                        <div className="p2">{item.createTime}</div>
                                        <div className="p3">
                                            <span>{item.typeName}</span>
                                        </div>
                                    </div>
                                </li>
                            )
                        })
                    }                
                </ul>
                {/* 分页 */}
                <div className="page">
                    <Pagination current={this.startPage} pageSize={this.pageSize} onChange={this.getBaseData.bind(this)} total={this.dataTotal} />
                </div>  
            </div>:
            <div className="draftBoxNodata">
                <img src="../static/com/blank5.png" />
                <p>页面暂无数据</p>
            </div>
        )
    }
}



export default Content




