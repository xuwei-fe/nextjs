import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {message} from 'antd';
import Router from 'next/router'
import Content from './Content'
import fetchData from '/utils/fetchData'
class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            height: "",
            onewm:false,
            url:'',
            isGetEwm:false,
            showAndroidEwm:false,
            showIosEwm:false,
        }
        this.openewmFn = this.openewmFn.bind(this);
        
    }
    upDateValue(value) {
       
    }
    downAnd(e){
        e.stopPropagation()
        clearInterval(this.timer)
        this.setState({
            showAndroidEwm:true,
            showIosEwm:false,
            onewm:false,
            isGetEwm:false,
        })
    }
    downIos(e){
        e.stopPropagation()
        clearInterval(this.timer)
        this.setState({
            showIosEwm:true,
            showAndroidEwm:false,
            isGetEwm:false,
            onewm:false
        })
    }
    openewmFn(e) {
        e.stopPropagation()
        this.setState({
            isGetEwm:true,
            showIosEwm:false,
            showAndroidEwm:false
        })
        fetchData.get('/auth/unsecured/pc/get/login/qrCode')
        .then((response)=>{
            this.setState({
                isGetEwm:false,
                onewm:true,
                url:'data:image/png;base64,'+response.respData.qrCodeBase64String,
                qrCodeId:response.respData.qrCodeId
            })
            this.timer=setInterval(()=>{
                fetchData.get('/auth/unsecured/pc/verify/login',{
                    params: {
                        qrCodeId:response.respData.qrCodeId
                    }
                })
                .then((response)=>{
                    if(response.dataCode==1){
                        clearInterval(this.timer);
                        this.setState({
                            onewm:true,
                            url:'data:image/png;base64,'+response.respData.qrCodeBase64String
                        })
                        sessionStorage.setItem('user', JSON.stringify(response.respData))
                        localStorage.removeItem("ueditor_preference");
                        Router.push({
                            pathname: '/addArticle',
                          })
                        
                    }
                    else if(response.dataCode==4){
                        //二维码失效
                        clearInterval(this.timer);
                        message.error("二维码已失效，请重新获取！")
                        //this.openewmFn()
                        this.setState({
                            isGetEwm:false,
                            onewm:false
                        })
                    }
                    
                })
                .catch((error)=> {
                  console.log(error);
                });
            },3000)
        })
        .catch((error)=> {
          console.log(error);
        });
        

    }
    closeewmFN(){
        this.setState({
            showAndroidEwm:false,
            showIosEwm:false
        })
        if(this.state.isGetEwm){
           return 
        }
        clearInterval(this.timer)
        this.setState({
            isGetEwm:false,
            onewm:false
        })
    }
    stopUp(e){//阻止冒泡
        e.stopPropagation()
    }
    componentDidMount () {
        
        this.setState({
            height:window.innerHeight
        })
    }
    render () {
        return (
            <div>
                <Content 
                    stopUp={this.stopUp} 
                    url={this.state.url} 
                    isGetEwm={this.state.isGetEwm} 
                    downAnd={this.downAnd.bind(this)} 
                    downIos={this.downIos.bind(this)} 
                    showAndroidEwm={this.state.showAndroidEwm}
                    showIosEwm={this.state.showIosEwm}
                    height={this.state.height} 
                    onewm={this.state.onewm} 
                    openewmFn={this.openewmFn} 
                    closeewmFN={this.closeewmFN.bind(this)}
                />
            </div>
        )
    }
}
const mapStateToProps = this.props
//reducer
const mapDispatchToProps = (dispatch) => {
    return {
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(About)