import React from 'react'
import 'styles/index/index.scss'
import { Spin} from 'antd';
import Link from 'next/link'
const Content = (props) => {
    const { 
        height,
        openewmFn,
        closeewmFN,
        onewm,url,
        downAnd,
        downIos,
        showAndroidEwm,
        showIosEwm,
        isGetEwm,
        stopUp
    } = props;
    return (
        <div className="wrap" style={{"height":height}} onClick={closeewmFN}>
            <div className="headerWrap clearfix">
                <div className="logo"></div>
                <div className="navlist">
                    <Link href='/index'>
                        <span className="on">首页<i></i></span>
                    </Link>
                    <Link href='/greatgal'>
                        <span>成为大咖<i></i></span>
                    </Link>
                    <Link href='/userProtocol'>
                        <span>用户协议<i></i></span>
                    </Link>
                    <Link href='/about'>
                        <span>关于我们<i></i></span>
                    </Link>
                </div>
            </div>
            <div className="contentWrap clearfix">
                <div className="left clearfix">
                    <p className="p">殡葬业第一个<br/>交流、学习、分享平台</p>
                    <div className="div" onClick={downAnd}>
                        {
                            showAndroidEwm?
                            <div className="ewm"><img src="/static/index/downEwm.png"/></div>
                            :''
                        }
                        <img className="icon" src="/static/index/android.png"/>扫码下载
                    </div>
                    {/* <div className="div" onClick={downIos}>
                        {
                            showIosEwm?
                            <div className="ewm"><img src="/static/index/downEwm.png"/></div>
                            :''
                        }
                        <img className="icon" src="/static/index/ios.png"/>IOS
                    </div>                                       */}
                    <div className="ewmDiv" onClick={stopUp}>
                        <Spin spinning={isGetEwm}>  
                            <div className="divThree" onClick={openewmFn}>
                                <img className="icon" src="/static/index/homeQR.png" />扫码登录
                            </div>
                        </Spin> 
                        <img className={onewm?"showImg showImgBlock":"showImg"}  src={onewm?url:"/static/index/ewm.png"}  />
                    </div>                                      
                </div>
                <div className="right" >
                    <img src="/static/index/home_phone.png" alt=""/>
                </div>
            </div>           
        </div>
    )
}
export default Content