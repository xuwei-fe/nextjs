import React from 'react'
import Link from 'next/link'
import 'styles/manageAcount/manageAcount.scss'
import fetchData from '/utils/fetchData'
import Router from 'next/router'
import Uploadimg from './uploadimg'
import { Input,message } from 'antd';


class Content extends React.Component{
    constructor(props) {
        super(props)
        this.state={
            realName:"",
            companyName:"",
            positionName:"",
            certificateImage:"",
            headImage:"",
            imageflag:false,//上传图片裁剪框
            uploadType:1,//上传类型
        }
        this.dataTotal="";//总条数
        this.startPage = 1;
        this.pageSize = 6//每页条数
    }

    componentDidMount(){
        if(!JSON.parse(sessionStorage.getItem('user'))){
            Router.push({
                pathname: '/index',
            })
        }
        this.getBaseData();
    }
    getBaseData(){
        fetchData.get('/account/pc/getAccountInfo',
            {headers:{"Authorization":"bearer "+JSON.parse(sessionStorage.getItem('user')).access_token+""}}
        )
        .then((response)=>{
            this.setState({
                realName:response.respData.realName,
                companyName:response.respData.companyName,
                positionName:response.respData.positionName,
                certificateImage:response.respData.certificateImage,
                headImage:response.respData.headImage
            })
        })
        .catch((error)=> {
        });
    }
    closeFn(){
        this.setState({
            imageflag:false
        });
    }
    urlFn(url){
        if(this.state.uploadType == 1){
            this.setState({
                imageflag:false,
                headImage:url
            });
        }else{
            this.setState({
                imageflag:false,
                certificateImage:url
            });
        }
    }
    nameChange(event){
        this.setState({
            realName:event.target.value
        })
    }
    companyChange(event){
        this.setState({
            companyName:event.target.value
        })
    }
    positionChange(event){
        this.setState({
            positionName:event.target.value
        })
    }
    saveData(){
        if(this.state.realName ==""){
            message.error('请输入姓名');
            return;
        }
        if(this.state.companyName ==""){
            message.error('请输入公司');
            return;
        }
        if(this.state.positionName ==""){
            message.error('请输入职位');
            return;
        }
        if(this.state.headImage ==""){
            message.error('请上传头像');
            return;
        }
        var data = {
            realName:this.state.realName,
            companyName:this.state.companyName,
            positionName:this.state.positionName,
            // certificateImage:this.state.certificateImage,
            headImage:this.state.headImage,
        }
        fetchData.post('/account/pc/saveAccountInfo',data,
            {headers:{"Authorization":"bearer "+JSON.parse(sessionStorage.getItem('user')).access_token+""}}
        )
        .then((response)=>{
            message.success('保存成功');
            var data = JSON.parse(sessionStorage.getItem('user'))
            data.nickname = this.state.realName;
            data.headImage = this.state.headImage;
            sessionStorage.setItem("user",JSON.stringify(data))
            setTimeout(()=>{
                history.go(0);               
            },1000)

        })
        .catch((error)=> {
        });
    }
    changeHeaderImg(type){

        this.setState({
            uploadType:type,
            imageflag:true
        });
    }
    render(){
        return (
            <div className="manageAcountContent">
                <ul className="left">
                    <li className="li">
                        <span>姓名</span>
                        <div className="input">
                            <Input onChange={this.nameChange.bind(this)}  value={this.state.realName}></Input>
                        </div>                    
                    </li>
                    <li className="li">
                        <span>公司</span>
                        <div className="input">
                            <Input onChange={this.companyChange.bind(this)} value={this.state.companyName}></Input>
                        </div>  
                    </li>
                    <li className="li">
                        <span>职位</span>
                        <div className="input">
                            <Input onChange={this.positionChange.bind(this)} value={this.state.positionName}></Input>
                        </div>
                    </li>
                    {/* <li className="li">
                        <span className="sp4">个人认证</span>  
                        <div className="div4" onClick={this.changeHeaderImg.bind(this,2)}><img src={this.state.certificateImage ?this.state.certificateImage :'/static/addarticle/baseHeader.png'} /></div>                     
                    </li> */}
                    <li className="li">
                        <div className="save" onClick={this.saveData.bind(this)}><div className="hoverBtn"></div>保存</div>                         
                    </li>
                </ul>
                <div className="right">
                    <div className="img"><img src={this.state.headImage ? this.state.headImage :'/static/addarticle/baseHeader.png' } /></div>
                    <div className="btn" onClick={this.changeHeaderImg.bind(this,1)}>更换头像</div>
                </div>
                <Uploadimg 
                    width={this.state.uploadType == 1 ?280:440} 
                    height={this.state.uploadType == 1 ?280:330} 
                    type={this.state.uploadType} 
                    url={this.state.uploadType == 1 ?this.state.headImage:this.state.certificateImage} 
                    visible={this.state.imageflag} 
                    closeModal={this.closeFn.bind(this)} 
                    urlFn={this.urlFn.bind(this)}
                />
            </div>
        )
    }
}

export default Content




