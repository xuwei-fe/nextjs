import React from 'react'
import Link from 'next/link'
import 'styles/about/about.scss'
const AboutContent = (props) => {
    return (
        <div className="AboutContent">
            <div className="left">
                <p className="p1">
                    这是一群由梦想不将就的人构成的团队。从一开始，我们就打算做点不一样的事情，去影响甚至改变这个将就的世界。我们选择了互联网作为实践梦想的第一步，我们同样信仰这个世界上有这样一群的你们，热衷互联网生活、有品位、乐分享，酷爱一切精致与新鲜，愿意为美好喝彩。
                </p>
                <p className="p2">
                    慧殡葬平台，我们改变世界的第一个翘板。它提供了以用户为中心的产品，给移动化平台带来了无限的可能性。无论你是如何发现久星。都将为它超乎想象的产品而感到惊喜。我们相信，它在用自己的方式，推动着互联网的变革，同时改变着我们每一个人的生活方式。
                </p>
                <p className="p3">
                    这就是久星，是关于我们的，因梦想而立的故事，从这一天开始，一切都不平常！
                </p>
                <ul className="ul">
                    <li><img src="../../static/about/about_Phone.png" />021-64020620</li>
                    <li><img src="../../static/about/about_Printer.png" />021-64976037</li>
                    <li><img src="../../static/about/about_Combined Shape.png" />17317753770</li>
                    <li><img src="../../static/about/about_Globus.png" />www.lifebook.xin</li>
                    <li><img src="../../static/about/about_Mail.png" />service@lifebook.xin</li>
                    <li><img src="../../static/about/about_about_.png" />上海闵行区景联路398号</li>
                </ul>
            </div>
            <div className="right">
                <div className="text"><img  src="../../static/about/aboutus.png" /></div>
                <div className="map"><img  src="../../static/about/map.png" /></div>                            
            </div>
        </div>
    )
}
export default AboutContent




