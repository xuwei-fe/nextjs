import React from 'react'
import Link from 'next/link'
import 'styles/greatgal/greatgal.scss'
import { Form, Input, Button,message} from 'antd';
const FormItem = Form.Item;
import fetchData from '/utils/fetchData'
class Greatgalform extends React.Component{
    constructor(props) {
        super(props)
    }
    handleSubmit = (e) => {
        e.preventDefault();
        // fetchData.get('http://192.168.1.130:8765/system/masterData/list', {
        //     params: {
        //       type: 1
        //     }
        //   })
        //   .then(function (response) {
        //     console.log(response);
        //   })
        //   .catch(function (error) {
        //     console.log(error);
        //   });
        var this_=this
        this.props.form.validateFields((err, values) => {
          if (!err) {
            fetchData.post('/content/unsecured/bigShotApplyFor/save',values)
            .then(function (response) {
                this_.props.form.resetFields();
                message.success(response.respMsg);
            })
            .catch(function (error) {
                console.log(error);
            });
          }
        });
      }
    render(){
        const { getFieldDecorator} = this.props.form;
        return (
            <div className="greatgalformwarp" style={{"height":this.props.height}}>
                <div className="ulbox">
                    <ul className="clearfix">
                        <li className="on">
                            <img src="/static/greatgal/daka1.png" alt=""/>
                            <p>顾问报酬</p>
                        </li>
                        <li>
                            <img src="/static/greatgal/daka2.png" alt=""/>
                            <p>高端人脉</p>
                        </li>
                        <li className="on">
                            <img src="/static/greatgal/daka3.png" alt=""/>
                            <p>个人品牌</p>
                        </li>
                        <li>
                            <img src="/static/greatgal/daka4.png" alt=""/>
                            <p>职业发展</p>
                        </li>
                    </ul>
                    <div className="formDiv">
                        <Form className="login-form" onSubmit={this.handleSubmit}>
                            <FormItem>
                                {getFieldDecorator('realName', {
                                    rules: [{ required: true, message: '请输入您的姓名' }],
                                })(
                                    <Input placeholder="姓名"></Input>
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('mobile', {
                                    rules: [{ required: true, message: '请输入您的手机号' },
                                    {
                                        pattern: /^1[3|4|5|7|8]\d{9}$/, message: '请输入正确的手机号',
                                    }],
                                })(
                                    <Input placeholder="手机号" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('companyName', {
                                    rules: [{ required: true, message: '请输入您的公司' }],
                                })(
                                    <Input placeholder="公司" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('positionName', {
                                    rules: [{ required: true, message: '请输入您的职位' }],
                                })(
                                    <Input placeholder="职位" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('cityName', {
                                    rules: [{ required: true, message: '请输入您的城市' }],
                                })(
                                    <Input placeholder="城市" />
                                )}
                            </FormItem>
                            <Button type="primary" htmlType="submit" className="login-button"><div className="hoverBtn"></div>提交</Button>
                        </Form>
                    </div>
                </div>
                
            </div>
        )
    }

    
}
Greatgalform = Form.create({})(Greatgalform);
export default Greatgalform