import React from 'react'
import 'styles/index.scss'
import 'styles/HandleLeft.scss'
import Link from 'next/link'
// 登录之后左侧功能栏
export default class HandleLeft extends React.Component {
    componentDidMount() {
        //flexible(window, document)
    }
    render() {
        const { isChoose } = this.props
        return (
            <div className="HandleLeft">
                <div className="title"><img src="../static/handleLeft/articeset.png"/>文章管理</div>
                <ul className="ul">
                    <Link href='/addArticle'>
                        <li className={ isChoose == 1 ? 'liChoose' : '' }>
                            <img src={ isChoose == 1 ? '../static/handleLeft/left1_hover.png' : '../static/handleLeft/left1.png' }/>新增文章
                        </li>
                    </Link>
                    <Link href='/myArticle'>
                        <li className={ isChoose == 2 ? 'liChoose' : '' }>
                            <img src={ isChoose == 2 ? '../static/handleLeft/left2_hover.png' : '../static/handleLeft/left2.png' }/>我的文章
                        </li>
                    </Link>
                    <Link href='/draftBox'>
                        <li className={ isChoose == 3 ? 'liChoose' : '' }>
                            <img src={ isChoose == 3 ? '../static/handleLeft/left3_hover.png' : '../static/handleLeft/left3.png' }/>草稿箱
                        </li>
                    </Link>
                </ul>
                <div className="title"><img src="../static/handleLeft/systemset.png"/>系统管理</div>
                <ul className="ul">
                    {/* <li className={ isChoose == 4 ? 'liChoose' : '' }>
                        <img src={ isChoose == 4 ? '../static/handleLeft/left4_hover.png' : '../static/handleLeft/left4.png' }/>我的素材
                    </li>                    */}
                    <Link href='/manageAcount'>
                        <li className={ isChoose == 5 ? 'liChoose' : '' }>
                            <img src={ isChoose == 5 ? '../static/handleLeft/left5_hover.png' : '../static/handleLeft/left5.png' }/>账号管理
                        </li>
                    </Link>
                    <Link href='/msgCenter'>
                        <li className={ isChoose == 6 ? 'liChoose' : '' }>
                            <img src={ isChoose == 6 ? '../static/handleLeft/left6_hover.png' : '../static/handleLeft/left6.png' }/>消息中心
                        </li>
                    </Link>
                </ul>
            </div>
        )
    }
}