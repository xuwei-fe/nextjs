import React from 'react';
import axios from 'axios'
class Ueditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
    };
  }
  componentDidMount(){
    setTimeout(()=>{
      this.initEditor()
    },800)
    
  }
  componentWillUnmount() {
    // 组件卸载后，清除放入库的id
    UE.delEditor(this.props.id);
  }
  initEditor() {
    const id = this.props.id;
    const ueEditor = UE.getEditor(this.props.id, {
      serverUrl:axios.defaults.baseURL+'content/unsecured/baidu/ueditor/exec',
      initialFrameWidth:'100%',  //初始化编辑器宽度,默认1000
      initialFrameHeight:this.props.height,  //初始化编辑器高度,默认320
      elementPathEnabled:false,///是否启用元素路径
      wordCount:true,//是否开启字数统计
      imagePopup:false,//图片操作的浮层开关，默认打开
      fontsize:[16, 18, 20],
      scaleEnabled:false,//是否可以拉伸长高,默认true(当开启时，自动长高失效)
      maximumWords:20000,// 允许的最大字符数
      catchRemoteImageEnable: false,
      toolbars: !this.props.toolbars ?[[
      'undo', //撤销
      'redo', //重做
      'bold', //加粗
      'indent', //首行缩进
      'italic', //斜体
      //'underline', //下划线
      'formatmatch', //格式刷
      'fontsize', //字号
      //'simpleupload', //单图上传
      'insertimage',
      // 'insertvideo', //视频
      'forecolor', //字体颜色
      'justifyleft', //居左对齐
      'justifycenter', //居中对齐
      'justifyright', //居右对齐			          
      'justifyjustify', //两端对齐
    ]]:[],
  });
    const self = this;
    ueEditor.ready((ueditor) => {
        
      if(ueEditor.execCommand( 'getlocaldata' )){
        console.log('缓存有数据');
        ueEditor.execCommand( 'drafts' );
      }else{
        console.log('缓存无数据');
        ueEditor.setContent(this.props.value);
      }
      // console.log(ueEditor.execCommand( 'getlocaldata' ));
      // ueEditor.execCommand( 'getlocaldata' );
      //ueEditor.execCommand( 'clearlocaldata' );
      // ueEditor.fireEvent("contentchange","asdasd")

      if (!ueditor) {
        UE.delEditor(id);
        self.initEditor();
      }

    })
  }
  render(){
    return (
      <div id={this.props.id} name="content" type="text/plain" style={{lineHeight:"0"}}></div>
    )
  }
}
export default Ueditor;

/*
//'anchor', //锚点
    'undo', //撤销
    'redo', //重做
    'bold', //加粗
    'indent', //首行缩进
    //'snapscreen', //截图
    'italic', //斜体
    'underline', //下划线
    //'strikethrough', //删除线
    //'subscript', //下标
    //'fontborder', //字符边框
    //'superscript', //上标
    'formatmatch', //格式刷
    //'source', //源代码
    //'blockquote', //引用
    //'pasteplain', //纯文本粘贴模式
    //'selectall', //全选
    //'print', //打印
  //   'preview', //预览
    //'horizontal', //分隔线
    //'removeformat', //清除格式
    //'time', //时间
    //'date', //日期
    //'unlink', //取消链接
    //'insertrow', //前插入行
    //'insertcol', //前插入列
    //'mergeright', //右合并单元格
    //'mergedown', //下合并单元格
    //'deleterow', //删除行
    //'deletecol', //删除列
    //'splittorows', //拆分成行
    //'splittocols', //拆分成列
    //'splittocells', //完全拆分单元格
    //'deletecaption', //删除表格标题
    //'inserttitle', //插入标题
    //'mergecells', //合并多个单元格
    //'deletetable', //删除表格
    //'cleardoc', //清空文档
    //'insertparagraphbeforetable', //"表格前插入行"
    //'insertcode', //代码语言
    //'fontfamily', //字体
    'fontsize', //字号
    //'paragraph', //段落格式
    'simpleupload', //单图上传
    //'insertimage', //多图上传
    //'edittable', //表格属性
    //'edittd', //单元格属性
    //'link', //超链接
    //'emotion', //表情
    //'spechars', //特殊字符
    //'searchreplace', //查询替换
    //'map', //Baidu地图
    //'gmap', //Google地图
    //'insertvideo', //视频
    //'help', //帮助
    'forecolor', //字体颜色
    //'backcolor', //背景色
    //'insertorderedlist', //有序列表
    //'insertunorderedlist', //无序列表
    //'fullscreen', //全屏
    //'directionalityltr', //从左向右输入
    //'directionalityrtl', //从右向左输入
    //'rowspacingtop', //段前距
    //'rowspacingbottom', //段后距
    //'pagebreak', //分页
    //'insertframe', //插入Iframe
    //'imagenone', //默认
    //'imageleft', //左浮动
    //'imageright', //右浮动
    //'attachment', //附件
    //'imagecenter', //居中
    //'wordimage', //图片转存
    //'lineheight', //行间距
    //'edittip ', //编辑提示
    //'customstyle', //自定义标题
    //'autotypeset', //自动排版
    //'webapp', //百度应用
    //'touppercase', //字母大写
    //'tolowercase', //字母小写
    //'background', //背景
    //'template', //模板
    //'scrawl', //涂鸦
    //'music', //音乐
    //'inserttable', //插入表格
    //'drafts', // 从草稿箱加载
          //'charts', // 图表
          'justifyleft', //居左对齐
          'justifycenter', //居中对齐
    'justifyright', //居右对齐			          
    'justifyjustify', //两端对齐
    ]],
*/