import React from 'react'
import Head from 'next/head'
import 'styles/index.scss'
import Router from 'next/router'
import { LocaleProvider } from 'antd';
import zhCN from 'antd/lib/locale-provider/zh_CN';
// import flexible from 'utils/flexible'

export default class BasicLayout extends React.Component {
    componentDidMount() {
        console.log(window);
        // window.baseUrl = "http://192.168.1.130:8765/"
        // window.baseUrl = "http://preapi.funeralchain.com:8765/"//预发布环境
        window.baseUrl = "http://api.funeralchain.com:8765/"//生产环境


        //flexible(window, document)
    }
    render() {
        const { children, title  } = this.props
        return (
            <LocaleProvider locale={zhCN}>
                <div>
                    <Head>
                        <title>{title}+"慧殡葬-国际殡葬指南，殡葬企业黄页，墓碑，碑形设计，景观设计，殡仪，陵园，公墓，殡葬用品，殡葬管理运营"</title>
                        <meta charSet='utf-8' />
                        <meta name="description" content='“慧殡葬”是殡葬行业内第一款综合资讯APP，让从业者在“慧殡葬”平台上一起学习交流分享、大咖解惑殡葬知识、洞悉景观建筑设计趋势、无边界社区交流、获取殡葬用品最新动态、拓展行业人脉资源。'/>
                        <meta name="keyword" content='慧殡葬,殡葬指南,殡葬,中国殡葬,殡葬黄页,墓碑,景观设计,殡仪,陵园,公墓,殡葬用品,殡葬管理运营'/>
                        <script src="http://mock.lifebook.xin/rap.plugin.js?projectId=8"></script>
                        <script type="text/javascript" charSet="utf-8" src="/static/js/jquery.js"> </script>
                    </Head>
                    {children}
                </div>
            </LocaleProvider>
        )
    }
}